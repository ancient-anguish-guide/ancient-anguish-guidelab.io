# Newbie Topics

This page contains miscellaneous newbie topics that don't merit their own page.

## Getting Help

Ancient Anguish has a few ways to get help if you need it:

1. Use the help files; type `help` to get a list of topics and then `help <topic>` for more details. In particular, `help newbie`, `help newbie2`, `help newbie3`, `help newbie4`, and `help newbie5` all have useful information for new players.
2. If you completed the tutorial, you should have a few items that will help:
    - A newbie guidebook (`read guidebook`)
    - A reference card (`read card`)
    - A small oak carving - this allows you to chat on the newbie helpline, where you can ask beginner questions. Type `nhelp` for more information.
    - If you are missing the guidebook or carving, you can go `2s` of the Ancient Inn of Tantallon to the Smithy and grab new ones.
3. Go to the Smithy `2s` of the Ancient Inn of Tantallon, `grab sprite` to get a helpful sprite, and then type `chessa help`.
4. Use sites like this one and the ones listed in the [Links](reference/links.md) page.    

## Maps

Maps can be invaluable for helping you visualize the game world and be more efficient about moving and exploring. As mentioned on the [Quick Start](quick-start.md) page, maps for each location can be downloaded from the [Anguish Research](https://sites.google.com/site/anguishresearch/home) site. The download also includes a Geography file that shows the in-game coordinates of almost every location in the game.

In addition, the downloaded content from that site contains an ASCII folder that has a full ASCII (character-based) map of the game world, Oerthe. This map can be used in conjunction with the `prompt` command, described below, to track your coordinates to help navigate the game world.

Within the game, you can also buy a map from Hanza's Map Shop (`5w, 3s, 1w` from the crossroads). The map comes in several sizes (`pocket`, `small`, `medium`, and `large`). To purchase a map, enter `buy <size>`. After purchasing a map, enter `read map` to see a display that looks like this:

![map image](images/read-map.png)

The player's location is marked with `< >` and shows the player centered on Tantallon (signified by `[]`). I always set `alias rm read map` so I can look at my map by just entering `rm`.

## Prompt

The prompt is displayed after every command you enter. By default, you see this:

```text
>
```

The prompt can be set to one of several preset configurations, or customized to exactly what you want. See `help prompt` or `prompt presets` for more information. You can use the prompt to quickly see valuable information about your character. If you enter `prompt on`, you will see this:

```text
 50: 50>
```

These values correspond to your hps and sps. Using `prompt preset coords` will display this:

```text
0,0
 50: 50>
```

The 0,0 correspond to your x, y coordinates in the game world. I customize my prompt with this command:

```text
prompt set |HP|/|COLOUR*bright green||MAXHP||SPACE||COLOUR*none||SP|/|COLOUR*bright green||MAXSP||SPACE||SESSIONXP||COLOUR*iyellow||SPACE||XCOORD||SPACE||YCOORD||COLOUR*none||SPACE|>|SPACE|
```

Now my prompt looks like this (plus colors in game):

```text
50/ 50  50/ 50 0 0 0 >
```

This prompt displays my hps, max hps, sps, max sps, experience earned during the current session, x, and y coordinates.

## Death

Ancient Anguish is a difficult game, and you will almost certainly die at some point. When you die, you come back in the game as a ghost until you visit the church and `pray`. Unfortunately, death does have a significant penalty to it; you will lose a level, experience, and skills. It can be gained back in time, but the penalty is significant enough that you should use great caution when exploring new areas and fighting new enemies. Try to avoid dying at all costs; there is no shame in running away from a battle.

## Wimpy

The `wimpy` command lets you set a threshold that has your character automatically run away during a battle. This can save your character's life if you are careless (or if you get disconnected!). It works as a percentage of your hps; set `wimpy 20` to automatically leave the room when you reach 20% of your max hps. There is a minor penalty when wimpy is triggered, but it is small compared to the cost of death. See `help wimpy` for more details.

## Quitting and Reboots

Most gear in Ancient Anguish does not persist once your character quits their current session. The same applies when the server is rebooted (once every 2 days). Each time this happens, you will need to obtain your gear again. While this may initially seem frustrating, the game is balanced around this mechanic. Most characters can get back a reasonable set of gear within a few minutes. Unlike items, coins are persistent between sessions and reboots.

## Dimensional Doors

Around Tantallon, you will find a number of "dimensional doors", or ddoors, that link between two rooms in the game. These are incredibly helpful in quickly traversing the world. They appear in the game as:

```text
A shimmering blue door.
```

To use them, `enter door` (or set `alias ed enter door` and then just do `ed`). These doors are created manually by helpful mages after each reboot, so they may not always be available immediately after a reboot. A full listing of the ddoors is availble on [this page](https://www.aawiki.net/wiki/Ddoors).