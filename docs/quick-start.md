# Quick Start

After you first log in to Ancient Anguish, you'll be prompted to create a character and select a race. Race options are Dwarf, Elf, Half-Elf, Human, and Orc. Your race will affect your maximum attributes, among other things, and different races are better suited for different classes. For purposes of this quick start guide, we'll start as an Orc and later join the Fighter class.

After choosing your race, you'll be prompted to choose one of these options:

```text
Which of the following statements would describe you the best?
        1       I am new to playing MUDs.
        2       I have played other MUDs before.
        3       I have played Ancient Anguish before, or have other characters.
        4       I am new to Ancient Anguish, but do not want new player assistance.
```

If you've never played Ancient Anguish before, I highly recommend options 1 or 2. This will take you through a short tutorial (10-15 minutes) that will teach you a few basic game commands. At the end of the tutorial, you will be able to choose which room to start in (`inn`, `fighter`, `cleric`, or `mage`). This guide will pick up where that tutorial left off.

If you chose one of the classes, you can immediately enter `join` to join that class. Joining a class will give you class-specific abilities that will make your character much stronger. Type `help <class name>` to learn more about your class. Iof you chose `inn`, we first need to navigate to one of the class halls to join.

## Navigating the Game World

The first room you start after logging in, or after choosing the `inn` option from the tutorial, is the Ancient Inn of Tantallon. You can recognize this room by the text, **"You stand in the common room of the Ancient Inn of Tantallon."**

Directions within AA are almost always given from the "crossroads", or the point in Tantallon where the primary east-west and north-sound roads cross. You can recognize this room on the map by the text, **"You stand at a major crossroads of the village."** The crossroads are located `s, 6e` from the Ancient Inn of Tantallon. Go ahead and navigate to the crossroads by entering `s` followed by `e` 6 times. From here, we can then move to the fighter class hall located `2n, w` from the crossroads (the same location as if you chose `fighter` from the tutorial). Once there, type `join`, and then `help fighters` to learn about the class. Directions from the crossroads to other nearby class halls are:

- Cleric - `3w, n, e, n, e`
- Fighter - `2n, w`
- Mage - `3w, s, w, u`

A detailed map of Tantallon and other game areas can be found on the [Anguish Research](https://sites.google.com/site/anguishresearch/home) site. I recommend visiting that site and downloading all of the content. After you download and unzip it, there is a MAPS folder with detailed game maps. These maps are invaluable for helping to navigate the game areas effectively.

## Gaining Experience

After joining your class, the next step will be to start improving your character by gaining experience and leveling up. Initially, there are two primary ways to gain experience:

1. Running delivery jobs for the Commodities Exchange (CX)
2. Killing enemies

## Running Delivery Jobs

The CX will give you delivery jobs to complete in and around Tantallon. These jobs will grant you both experience and coins. They are also useful for learning the map, but first, you need to get to the CX.  From the crossroads, go `2e, s, w` to arrive at the CX. From here, you first need to `read rules`. You then have two options:

1. `current` to list the current job available
2. `accept` to accept the job

Every few seconds, a new job will be offered. Your first few jobs will be limited to within Tantallon, so if Lou won't give you the job, wait a few seconds for another job to become available. Once you have accepted a job, navigate to the specified location, and type `deliver`. You should earn both some experience as well as coins. 

Full directions for every delivery job can be found on the [CX Directions](reference/cx-directions.md) page.

The CX is much more efficient than killing enemies at low levels. Once you've become accustomed to running CX jobs, you should be able to earn around 70 exp/minute. However, there are a few things to keep in mind:

1. If another player is also running CX, they will be taking up some of the jobs, so your efficiency will be greatly reduced.
2. The CX is only open from 6 AM - 9 PM in game time (type `time` to see the game time).
3. Around level 4-5, you will stop gaining any experience from CX deliveries. You can still earn coins.

In-game time advances 5 minutes for every 1 minute of real time, so if the CX is closed, you'll have to wait a while. You can kill some enemies while you wait.

## Killing Enemies

Killing enemies is the primary way to earn experience in Ancient Anguish. Before killing anything, you need to make sure you have the right equipment for your class. You can type `inventory` or `i` to see your equipment, `wield <weapon>` and `unwield <weapon>` to equip and remove weapons, and `wear <armour>` and `remove <armour>` to wear and remove armour. If you don't have any weapon or armour, you can visit the Tantallon Smithy to get some free newbie gear. From the crossroads, go `6w, s` and `say equipment`.

There are several areas designed for newbies. The first area I recommend exploring is the **Tantallon Park** (aka Small Park). From the crossroads, navigate `8w, s, enter park`. From the park entrance, go south, and then explore the area to find a variety of easy enemies to kill. Almost all of them are non-aggressive, meaning they won't attack you first, with the exception of a tiny lizard. 

When you find something you want to fight, enter `kill <target>` to begin combat. Combat takes place automatically over a series of rounds about 2 seconds apart. As your character improves, you will gain more abilities to use during combat. When your target dies, it may have loot on its corpse. You can `take all from corpse` to take its items, and then sell them back at one of the stores in Tantallon (for example, the Tantallon shop `1w, 1n` of the crossroads buys most items).

In the park, you can `search bushes` and `sit on bench` to find more creatures to fight, and `drink fountain` from the center room for light healing. There is also a room where you can `shake tree` to find some apples to eat for healing.

If you want a detailed map of the Tantallon Park, visit [Anguish Research](https://sites.google.com/site/anguishresearch/home) and download the content.

As a fighter, you can use some of your class abilities to kill faster. For example, you can use `defend none` and then `fury on` to sometimes hit twice each round. You can also do `defend berserk` to increase damage (although in exchange, you'll take more damage). Each class has different abilities, and their commands can be found in each class's help file (`help <classname>`).

Each weapon has an associated skill with it, and as you fight, you will gradually level up these skills. Type `skills` to see your current skill levels. Weapon skill improves your chance to hit an opponent (along with dexterity), but they also level quite slowly. It's a good idea to choose one weapon and focus on that weapon type for a while. To help with the early levels, you can visit Zhou's camp at `26n, e` from the crossroads. Here, type `train` and Zhou will train you to use the current weapon you have wielded in exchange for some gold. This greatly improves the speed at which you level that weapon, and this is usable through skill level 15. I highly recommend doing this for the early levels. You can verify whether the training is still helping you by entering `effects`.

## Aliases

One of the first things you'll want to do when playing Ancient Anguish is set up some aliases. Aliases are shortcuts for commands that you type frequently. For example, instead of typing `kill <enemy>`, you can set an alias so that you can type `k <enemy>`. These commands can save you a lot of time and typing. Here is a list of the first aliases I always set on a new character:

```text
alias k kill
alias c look at opponent
alias la look at
alias ed enter door
alias scs score session
alias tafc take all from corpse
alias tc take coins
alias ta take all
alias ka keep all
alias da drop all
alias sa sell all
```

More advanced features are also available with aliases. See `help alias` to learn more.

## Advancement

After a while, you should have earned enough experience to gain a level or advance your attributes. Return to your class hall and type `cost` to see how much experience it costs to advance. Type `advance` to gain a level or `advance <attribute>` to improve a specific attribute. I recommend improving all the attributes you can before advancing your level.

Now that you know the basics of the game, you should be ready to explore other newbie areas and learn more details about some of the game mechanics.
