# Links

For more information about Ancient Anguish, see these sites:

* [Ancient Anguish](http://www.anguish.org/) - The Ancient Anguish homepage.
* [Anguish Research](https://sites.google.com/site/anguishresearch) - The most comprehensive site about Ancient Anguish; it has information on almost every aspect of the game.
* [Ancient Anguish Wiki](https://aawiki.net/wiki/Main_Page) - A wiki about the game. Unfortunately, it no longer seems to be maintained.
