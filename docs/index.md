# Ancient Anguish Guide

Welcome to my guide to [Ancient Anguish](http://anguish.org)! Ancient Anguish is a text-based MUD (Multi-User Dungeon) that has been running for over 30 years.  After 30 years of development, the learning curve for the game can be quite steep, so this guide was created to help ease new players into the game.

To get started playing the game, you can connect to it using a telnet client application. I recommend the free software [MUSHClient](http://www.gammon.com.au/downloads/dlmushclient.htm), but any other client will also work. Use these details to connect:

- hostname: ancient.anguish.org
- port: 2222

If you don't want to download a client, you can play via one of these web-based interfaces:

- [Grapevine](https://grapevine.haus/games/AA)
- [MUD Portal](http://www.mudportal.com/play?host=ancient.anguish.org&port=2222) - Note this site is somewhat unstable.

After you get connected, visit the [Quick Start](quick-start.md) page to get started. The below videos can be used in conjunction with the Quick Start guide:

- [Ancient Anguish Introduction, Part 1](https://youtu.be/-P4bP_FDbE8)
- [Ancient Anguish Introduction, Part 2](https://youtu.be/0qqpu0cVyAA)
