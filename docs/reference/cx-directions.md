# CX Directions

This is a summary of directions for delivery jobs given to you from the CX as described in the [Quick Start Guide](../quick-start.md). The directions are divided into two lists:

1. Initial deliveries that you can get from Lou that stay within Tantallon or just outside of it.
2. More advanced deliveries you can get from Lou or Pembrook (located `1d` from Lou). These take you outside of Tantallon and are unlocked after doing 15-20 of the initial deliveries.

To get to the CX, go `2e 1s 1w` from the crossroads. It runs from 6 AM - 9 PM game time. If Lou and Pembrook are not present, the CX is closed and you will need to wait until it reopens.

## Initial Deliveries

|Location|Directions from Crossroads|
|-|-|
auction hall in tantallon|3s, 4w, 1s
bank in tantallon|3w, 1s, 1e
brawling arena in tantallon|1n, 1e, 1n
canticle offices west of tantallon|8w, enter hut
clerics' common church|3w, 1n, 1e, 1n, 1e
coachline office in tantallon|5w, 2s, 1w
fredd's equipment shop in tantallon|5w, 4s
hanza's map shop in tantallon|5w, 3s, 1w
ironman and willim's smithy in tantallon|6w, 1s
library in tantallon|3w, 1n, 1w
magic shop in tantallon|5w, 2s, 1e
paperboy|3w
pub in tantallon|3w, 2n
shop in tantallon|1w, 1n
tower of the magi in tantallon|3w, 1s, 1w, 1u
training academy in tantallon|1n, 1e

## Advanced Deliveries

|Location|Directions from Crossroads|
|-|-|
ancient bliss inn bar in nepeth|13n, 6w, 6n, 2w, 1n, 1w, 1n
ancient inn in the northeastern village|13n, 3e, 2n, 1e, 1s
bank in hobbitat|33n, 1ne, 2n, 1e, 1ne, 1n, 3w
barber shop in hobbitat|33n, 1ne, 1n, 1e, 1n, 1ne, 1n, 2e
butcher's shop in nepeth|13n, 6w, 6n, 3e, 1n, 1e
bard who sings at the ancient bliss inn in nepeth|13n, 6w, 6n, 2w, 1n, 1e
clan hut entrance in the scythe camp|9w, 5s, walk
constabulary in the northeastern village|13n, 3e, 2n, 2e, 1s
doctor's office in the dwarven mines|16n, 3w, 2n, 1nw, 1w, open w, 1w
eastroad inn north of tantallon|10n, 1e, enter
flower shop in nepeth|13n, 6w, 6n, 2w, 1s
goblin alchemist in ravel|12w, 1n, 1nw, 4w, 2n, 3w, 1n, enter
grandfather goblin at the goblin hut in ravel|12w, 1n, 1nw, 4w, 3n, enter, 1w
hall of audience in nepeth|13n, 6w, 10n, open n, 2n, open w, 1w
infirmary in the dwarven mines|16n, 3w, 2n, 1nw, 1w, 1n
knights of drin guildhall in nepeth|13n, 6w, 10n, open n, 3n
library in nepeth village|13n, 6w, 3n, 1e, 1n
lobby of the sheriff's office in nepeth|13n, 6w, 6n, 4e
nurses' station in the dwarven mines|16n, 3w, 2n, 1nw, 2n, open w, 1w
oterim the sage in the northeastern village|13n, 3e, 2n, 2e, 1n
ranger camp|31n, 9w, 10n, 16e, 1s, enter camp
reception desk at the eastroad inn north of tantallon|10n, 1e, enter, 1e
shop in hobbitat|33n, 1ne, 1e
shop in the scythe camp|9w, 5s, walk, 1w, 1n, 1w, 1s
store in hobbitat|33n, 1ne, 1n, 2e, 1se, 1e
tavern in dalair|16w, 7n
tobacco seller in the scythe camp|9w, 5s, walk, 1w, 1n, 2w, 1n
