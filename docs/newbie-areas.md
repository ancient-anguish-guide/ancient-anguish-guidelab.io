# Newbie Areas

There are several areas in the game designed for newbies and low-level players. This page lists a few of the best ones. These are listed in approximate order of difficulty.

- **Tantallon Park**
    - Directions: 8w, s, enter park
    - Coordinates: -1, -1
    - `drink fountain` and eat apples for healing
- **Haunted Manor**
    - Directions: 8n, 1e, 1ne
    - Coordinates: 0, 2
    - Go 2n, up, w, and `sleep` for healing. Ignore the nightmares; they're only flavor text.
- **Nepeth Arena**
    - Directions: 13n, 6w, 8n, 1w
    - Coordinates: -6, 9
    - A couple of weapons dropped here easy to gain skills with.
- **Thicket**
    - Directions: 17w, 6s, 1ne, crawl into thicket
    - Coordinates: -10, -6
    - Avoid fighting Harlan at low levels.
- **Haunted Shipwreck**
    - Directions: 29n, 5e, enter ship
    - Coordinates: 5, 23
    - Free healing is available here.
    - Avoid the captain and spirits at low levels.
